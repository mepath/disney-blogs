<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>www.disneymoviecollections.com</title>
<script type="text/javascript" src="http://www.disneymoviecollections.com/wp-content/themes/Cartoon_Group2-1/script.js"></script>
<link rel="stylesheet" href="http://www.disneymoviecollections.com/wp-content/themes/Cartoon_Group2-1/style.css" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" href="http://www.disneymoviecollections.com/wp-content/themes/Cartoon_Group2-1/style.ie6.css" type="text/css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="http://www.disneymoviecollections.com/wp-content/themes/Cartoon_Group2-1/style.ie7.css" type="text/css" media="screen" /><![endif]-->
<link rel="alternate" type="application/rss+xml" title="www.disneymoviecollections.com RSS Feed" href="http://www.disneymoviecollections.com/feed/" />
<link rel="alternate" type="application/atom+xml" title="www.disneymoviecollections.com Atom Feed" href="http://www.disneymoviecollections.com/feed/atom/" /> 
<link rel="pingback" href="http://www.disneymoviecollections.com/xmlrpc.php" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.disneymoviecollections.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.disneymoviecollections.com/wp-includes/wlwmanifest.xml" /> 
<link rel='index' title='www.disneymoviecollections.com' href='http://www.disneymoviecollections.com/' />

	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body>
<div id="art-main">
<div class="art-sheet">
    <div class="art-sheet-tl"></div>
    <div class="art-sheet-tr"></div>
    <div class="art-sheet-bl"></div>
    <div class="art-sheet-br"></div>
    <div class="art-sheet-tc"></div>
    <div class="art-sheet-bc"></div>
    <div class="art-sheet-cl"></div>
    <div class="art-sheet-cr"></div>
    <div class="art-sheet-cc"></div>
    <div class="art-sheet-body">
<div class="art-header">
    <div class="art-header-jpeg"></div>
<div class="art-logo">
<h1 id="name-text" class="art-logo-name">
        <a href="http://www.disneymoviecollections.com/">www.disneymoviecollections.com</a></h1>
    <div id="slogan-text" class="art-logo-text">
        </div>
</div>

</div>
<div class="art-nav">
	<div class="l"></div>
	<div class="r"></div>
	<ul class="art-menu">
		<li><a class="active" href="http://www.disneymoviecollections.com"><span class="l"></span><span class="r"></span><span class="t">Home</span></a></li><li class="page_item page-item-3"><a href="http://www.disneymoviecollections.com/About/" title="About"><span class="l"></span><span class="r"></span><span class="t">About</span></a></li>
	</ul>
</div>
<div class="art-content-layout">
    <div class="art-content-layout-row">
<div class="art-layout-cell art-content">

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2013/08/10/how-many-people-does-it-take-to-animate-a-movie/" rel="bookmark" title="Permanent Link to How Many People Does It Take To Animate A Movie?">
How Many People Does It Take To Animate A Movie?</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
August 10th, 2013 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>There is not a simple answer to how many people it takes to create an animated motion picture.  It used to take hundreds of people in the past to animate a movie.  Each person would have a single task such as coloring a cell, drawing a character or painting a background.  This was in addition to the dozens of people who worked on the script, developed the characters and created storyboards that reflected the narrative.</p>
<p>Modern technologies have reduced <a href="http://www.disneymoviecollections.com/2013/08/10/how-many-people-does-it-take-to-animate-a-movie/#more-13" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2013/06/04/what-is-the-technology-behind-disney-animation/" rel="bookmark" title="Permanent Link to What Is The Technology Behind Disney Animation?">
What Is The Technology Behind Disney Animation?</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
June 4th, 2013 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>Back when Disney started, the order of the day was hand-drawn animation, and that prevailed for decades.  However, with the advent of computers, these filmmakers learned how to use current technology to their advantage in crafting some of the world&#8217;s most beloved movies.</p>
<p>That&#8217;s not to say that there is no longer any artistic talent involved with creating these beloved movies.  No, these artists are just as talented as ever, but the computer is now able to fill in gaps that previously had to be filled by underling animators.  Hence, making an animated <a href="http://www.disneymoviecollections.com/2013/06/04/what-is-the-technology-behind-disney-animation/#more-12" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2013/03/27/how-disney-animation-has-changed-over-time/" rel="bookmark" title="Permanent Link to How Disney Animation Has Changed Over Time">
How Disney Animation Has Changed Over Time</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
March 27th, 2013 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>How Disney Animation Has Changed Over Time</p>
<p>Disney animation has changed in numerous ways. First, its technology has been radically updated over time. Disney animators and inkers once did it all by hand. They sketched each character, drew and painted each frame. Then, in the early 1960&#8242;s, a Xeroxing process allowed for mechanical copying so that, for instance, animators could photocopy each dog&#8217;s spots in &#8220;101 Dalmatians&#8221; (1961), rather than draw each spot over and over. <a href="http://www.disneymoviecollections.com/2013/03/27/how-disney-animation-has-changed-over-time/#more-11" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2012/08/02/the-princess-and-the-frog-a-return-to-form/" rel="bookmark" title="Permanent Link to The Princess and the Frog &#8211; A Return to Form">
The Princess and the Frog &#8211; A Return to Form</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
August 2nd, 2012 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>Disney has been churning out some truly remarkable films for decades and if you get the Disney Channel through services like www.rasertech.com, then you probably know what I&#8217;m talking about. However, it wasn&#8217;t all good and there was a time in Disney&#8217;s past when it seemed like the company had really lost their touch. It all happened around what some people call the Michael Eisner era in which Disney was described as becoming nothing more than a soulless corporation bent on cashing in on anything it could smear on the masses. During this time, among other poor decisions, Eisner decided that traditional, hand-drawn animation was obsolete and fired thousands of Disney animation veterans.</p>
<p>This inevitably led to a mediocre cavalcade of uninspired, computer generated works that nearly bankrupted the company. Eventually Eisner stepped down and Disney rehired all of their animators and adopted an approach which was a return to form in their storytelling and visualizations. This started with the Princess and the Frog and has been going strong ever since. For those of you who maybe did not see this film, the Princess and the Frog is a brilliant and heartwarming romp with a unique culture and flavor all its own. A must-see.</p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2012/02/24/five-best-animated-disney-heroes-of-all-time/" rel="bookmark" title="Permanent Link to Five Best Animated Disney Heroes of All Time">
Five Best Animated Disney Heroes of All Time</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
February 24th, 2012 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>5) Prince Phillip- Sleeping Beauty. This guy has it all. He has looks, honor and fighting prowess, not to mention bravery. He comes to Aurora&#8217;s aid despite the odds and faces an evil witch with nothing but a sword and shield.</p>
<p>4) Simba- The Lion King. Although at first Simba seems to have abandoned his role as king, he is quickly persuaded to come to the aid of his friends and family. He defeats the hyenas and Scar with little help. <a href="http://www.disneymoviecollections.com/2012/02/24/five-best-animated-disney-heroes-of-all-time/#more-8" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2012/02/21/five-best-animated-disney-heroines-of-all-time/" rel="bookmark" title="Permanent Link to Five Best Animated Disney Heroines of All Time">
Five Best Animated Disney Heroines of All Time</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
February 21st, 2012 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>Disney Heroines are aplenty, and picking out just the five best animated Disney Heroines of all time is sure to leave out some childhood favorites. The women in Disney&#8217;s animated movies are strong and worth looking up to. Belle, from Beauty and the Beast, is one of the best animated Disney heroines as she evokes caring and beauty. She teaches the beast love, and finds the real man underneath. She never looks down on anyone.</p>
<p>Mulan risks her own life to save her family. She is definitely a top five heroine. Selflessness makes her someone to look up to. Cinderella <a href="http://www.disneymoviecollections.com/2012/02/21/five-best-animated-disney-heroines-of-all-time/#more-7" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2012/02/17/the-history-of-disney-animation-domination/" rel="bookmark" title="Permanent Link to The History of Disney Animation Domination">
The History of Disney Animation Domination</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
February 17th, 2012 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>Walt Disney made his first animated cartoons in the 1930s. Even if a modern viewer has never seen Steamboat Willie, he will have heard of it if he is an animation fan. Steamboat Willie is when Mickey Mouse and Donald Duck made their first appearance.   Bugs Bunny would make his first appearance a few years later.  Bugs Bunny might not have made an appearance at all if Mickey Mouse had not achieved great commercial success.</p>
<p>Disney&#8217;s domination of the Animation industry started when the studio <a href="http://www.disneymoviecollections.com/2012/02/17/the-history-of-disney-animation-domination/#more-6" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2012/02/14/five-best-non-animated-disney-movies-of-all-time/" rel="bookmark" title="Permanent Link to Five Best Non-Animated Disney Movies of All Time">
Five Best Non-Animated Disney Movies of All Time</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
February 14th, 2012 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>As soon as the name &#8220;Disney&#8221; is mentioned the first thing that pops into my mind is Mickey Mouse and the cast of animated characters he&#8217;s associated with; Minnie, Donald, Goofy, Pluto and more. Some wonderful memories I must say. Walt Disney was truly the &#8220;Father of Animation&#8221;. However, Disney has also produced some of the best family oriented non-animated movies ever made. Here&#8217;s a quick rundown of the Top 5.</p>
<p>1Swiss Family Robinson<br />
The Robinson family find themselves shipwrecked on a deserted island. The family comes together to adapt to their <a href="http://www.disneymoviecollections.com/2012/02/14/five-best-non-animated-disney-movies-of-all-time/#more-5" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>

<div class="art-post">
    <div class="art-post-body">
<div class="art-post-inner art-article">
<div class="art-postmetadataheader">
<h2 class="art-postheader">
<a href="http://www.disneymoviecollections.com/2012/02/13/five-best-animated-disney-movies-of-all-time/" rel="bookmark" title="Permanent Link to Five Best Animated Disney Movies of All Time">
Five Best Animated Disney Movies of All Time</a>
</h2>

</div>
<div class="art-postheadericons art-metadata-icons">
February 13th, 2012 | Author: <a href="http://www.disneymoviecollections.com/author/admin/" title="Posts by admin">admin</a>
</div>
<div class="art-postcontent">
    <!-- article-content -->

          <p>Disney has been delighting audiences of children and adults for years with their animated movies. Everybody has a favorite Disney movie; the following are five of the top animated Disney movies of all time:</p>
<p>1. The Lion King &#8211; this wildly popular movie features a lion prince named Simba in Africa who must struggle to find this place in his kingdom; the movie won several awards for music and was also turned into a successful Broadway musical</p>
<p>2. Toy Story &#8211; this <a href="http://www.disneymoviecollections.com/2012/02/13/five-best-animated-disney-movies-of-all-time/#more-4" class="more-link">Read the rest of this entry &raquo;</a></p>
                  
    <!-- /article-content -->
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
Posted in Uncategorized | <span>Comments Closed</span>
</div>

</div>

</div>

		<div class="cleared"></div>
    </div>
</div>


</div>
<div class="art-layout-cell art-sidebar1">      
<div class="art-vmenublock">
    <div class="art-vmenublock-body">
<div class="art-vmenublockheader">
     <div class="t">Categories</div>
</div>
<div class="art-vmenublockcontent">
    <div class="art-vmenublockcontent-body">
<!-- block-content -->
<ul class="art-vmenu">
<li>No categories</li></ul>

<!-- /block-content -->

		<div class="cleared"></div>
    </div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-block">
    <div class="art-block-body">
<div class="art-blockheader">
     <div class="t">Search</div>
</div>
<div class="art-blockcontent">
    <div class="art-blockcontent-body">
<!-- block-content -->
<form method="get" name="searchform" action="http://www.disneymoviecollections.com/">
<input type="text" value="" name="s" style="width: 95%;" />
<span class="art-button-wrapper">
	<span class="l"> </span>
	<span class="r"> </span>
	<input class="art-button" type="submit" name="search" value="Search" />
</span>
</form>
<!-- /block-content -->

		<div class="cleared"></div>
    </div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-block">
    <div class="art-block-body">
<div class="art-blockheader">
     <div class="t">Categories</div>
</div>
<div class="art-blockcontent">
    <div class="art-blockcontent-body">
<!-- block-content -->
<ul>
  <li>No categories</li></ul>
<!-- /block-content -->

		<div class="cleared"></div>
    </div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-block">
    <div class="art-block-body">
<div class="art-blockheader">
     <div class="t">Archives</div>
</div>
<div class="art-blockcontent">
    <div class="art-blockcontent-body">
<!-- block-content -->
     
      <ul>
      	<li><a href='http://www.disneymoviecollections.com/2013/08/' title='August 2013'>August 2013</a></li>
	<li><a href='http://www.disneymoviecollections.com/2013/06/' title='June 2013'>June 2013</a></li>
	<li><a href='http://www.disneymoviecollections.com/2013/03/' title='March 2013'>March 2013</a></li>
	<li><a href='http://www.disneymoviecollections.com/2012/08/' title='August 2012'>August 2012</a></li>
	<li><a href='http://www.disneymoviecollections.com/2012/02/' title='February 2012'>February 2012</a></li>
      </ul>
    
<!-- /block-content -->

		<div class="cleared"></div>
    </div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-block">
    <div class="art-block-body">
<div class="art-blockheader">
     <div class="t">Bookmarks</div>
</div>
<div class="art-blockcontent">
    <div class="art-blockcontent-body">
<!-- block-content -->
<ul>
            </ul>
<!-- /block-content -->

		<div class="cleared"></div>
    </div>
</div>

		<div class="cleared"></div>
    </div>
</div>

</div>
    </div>
</div>
<div class="cleared"></div>


<div class="art-footer">

    <div class="art-footer-inner">

        <a href="http://localhost:8080/wordpress/?feed=rss2&amp;preview=1&amp;template=Cartoon_Group2&amp;stylesheet=Cartoon_Group2" class="art-rss-tag-icon" title="RSS"></a>

        <div class="art-footer-text">

<p>
<center><strong> &copy; 2015 www.disneymoviecollections.com </strong></center>
</p>

</div>

    </div>

    <div class="art-footer-background">

    </div>

</div>



		<div class="cleared"></div>

    </div>

</div>

<div class="cleared"></div>

<p class="art-page-footer">


</p>



</div>





<div>

</div>


<?php get_footer(); ?>
