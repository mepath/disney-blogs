<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Disney Cruise Line News</title>
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="http://dclnews.com/xmlrpc.php">
		<link rel="alternate" type="application/rss+xml" title="Disney Cruise Line News &raquo; Feed" href="http://dclnews.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Disney Cruise Line News &raquo; Comments Feed" href="http://dclnews.com/comments/feed/" />
<link rel='stylesheet' id='disneynewsroom-css'  href='http://static.dclnews.com/wp-content/themes/disneynewsroom/css/default.css?ver=20150326164127' type='text/css' media='screen' />
<link rel='stylesheet' id='disneynewsroom-print-css'  href='http://static.dclnews.com/wp-content/themes/disneynewsroom/css/print.css?ver=20150326164127' type='text/css' media='print' />
<script type='text/javascript' src='http://static.dclnews.com/wp-content/themes/disneynewsroom/js/libs/modernizr-2.8.1-min.js?ver=2.6.2'></script>
<script type='text/javascript' src='http://static.dclnews.com/wp-includes/js/jquery/jquery.js?ver=1399037356'></script>
<script type='text/javascript' src='http://static.dclnews.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1374607706'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://dclnews.com/xmlrpc.php?rsd" />
<link rel='shortlink' href='http://dclnews.com/' />
<!-- voce_seo -->
<meta name="robots" content="index,follow" />
<link rel="canonical" content="http://dclnews.com/" />
<meta property="og:url" content="http://dclnews.com/" />
<meta property="og:site_name" content="Disney Cruise Line News" />
<meta property="og:locale" content="en_us" />
<meta property="og:title" content="Disney Cruise Line News" />
<meta property="og:type" content="article" />
<meta name="twitter:url" content="http://dclnews.com/" />
<meta name="twitter:description" content="" />
<meta name="twitter:title" content="Disney Cruise Line News" />
<meta name="twitter:card" content="summary" />
<!-- end voce_seo -->
<link rel="shortcut icon" type="image/x-icon" href="http://static.dclnews.com/wp-content/uploads/sites/4/2012/12/favicon.ico" /><style>#masthead nav h1{width:270px;height:56px;background-size:270px 56px;background-image:url(http://static.dclnews.com/wp-content/uploads/sites/4/2014/10/dclnews_logo.png);}@media (max-width:47.9375em){#masthead nav h1{width:175px;height:36px;background-size:175px 36px;}}@media only screen and (-webkit-min-device-pixel-ratio:2),only screen and (min--moz-device-pixel-ratio:2),only screen and (-o-min-device-pixel-ratio:2 / 1),only screen and (min-device-pixel-ratio:2),only screen and (min-resolution:192dpi),only screen and (min-resolution:2dppx){#masthead nav h1{background-image:url(http://static.dclnews.com/wp-content/uploads/sites/4/2014/10/dclnews_logo2x.png);}}body{background-image:url(http://static.dclnews.com/wp-content/uploads/sites/4/2015/02/DCL2_news_bg_template.jpg);}body{background-color:#293872;}</style>		<link href='http://fonts.googleapis.com/css?family=Signika:400,700,300,600' rel='stylesheet' type='text/css'>
		<!--[if (gte IE 6)&(lte IE 8)]>
			<script type="text/javascript" src="http://static.dclnews.com/wp-content/themes/disneynewsroom/js/libs/selectivizr-min.js"></script>
			<script type="text/javascript" src="http://static.dclnews.com/wp-content/themes/disneynewsroom/js/libs/html5shiv.js"></script>
			<script type="text/javascript" src="http://static.dclnews.com/wp-content/themes/disneynewsroom/js/libs/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="home page page-id-4989 page-template-default">
		<header role="banner" id="masthead">
			<div class="container">
				<nav class="navbar">
					<div class="container-fluid">
						<div class="navbar-header">
							<a class="navbar-brand" target="_blank" href="http://disney.com/">Visit Disney.com</a>
						</div>
						<div class="navbar-right visible-lg visible-md" id="desktop-search">
							<form role="search" method="get" class="search-form" action="http://dclnews.com/">
	<div class="input-group input-group-sm">
		<span class="input-group-btn">
			<select class="selectpicker form-control search-form-control" name="post_type">
				<option value="">All</option>
				<option value="post" >Releases</option><option value="video" >Videos</option><option value="attachment" >Photos</option><option value="gallery" >Galleries</option><option value="fact-sheet" >Fact Sheets</option><option value="multimedia_kit" >Multimedia Kits</option>			</select>
		</span>
		<input type="search" class="form-control" placeholder="Search &hellip;" value="" name="s" />
		<span class="input-group-btn">
			<button class="btn btn-default"><span class="glyphicon newsroomicons-search"></span></button>
		</span>
	</div>

</form>
						</div>
						<div class="btn-group other-newsrooms navbar-right hidden-xs">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Other Newsrooms <span class="caret"></span></button>
							<ul id="menu-other-newsrooms" class="dropdown-menu"><li id="menu-item-12801" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12801"><a href="http://disneylandnews.com">Disneyland News</a></li>
<li id="menu-item-12800" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12800"><a href="http://wdwnews.com">Walt Disney World News</a></li>
<li id="menu-item-12925" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12925"><a href="http://disneyaulaninews.com">Disney Aulani News</a></li>
<li id="menu-item-12802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12802"><a href="http://disneysportsnews.com">Disney Sports News</a></li>
</ul>						</div>
						<ul class="nav navbar-nav navbar-right navbar-login">
			<li><a href="http://dclnews.com/wp-login.php">Login</a></li>
		<li><a href="http://dclnews.com/register">Register</a></li>
	</ul>					</div>
				</nav>

				<nav class="navbar" role="navigation">
					<div class="navbar-header">
						<a href="http://dclnews.com">
							<h1>Disney Cruise Line News</h1>
						</a>
					</div>
					<div class="navbar-right" id="main-navbar-right">

						<button type="button" id="search-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tablet-search" data-parent="#main-navbar-right">
							<span class="sr-only">Toggle search</span>
							<span class="glyphicon newsroomicons-search"></span>
						</button>
						<button type="button" id="navbar-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topnav-menu" data-parent="#main-navbar-right">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar top-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="collapse navbar-collapse navbar-right" id="tablet-search">
							<form role="search" method="get" class="search-form" action="http://dclnews.com/">
	<div class="input-group input-group-sm">
		<span class="input-group-btn">
			<select class="selectpicker form-control search-form-control" name="post_type">
				<option value="">All</option>
				<option value="post" >Releases</option><option value="video" >Videos</option><option value="attachment" >Photos</option><option value="gallery" >Galleries</option><option value="fact-sheet" >Fact Sheets</option><option value="multimedia_kit" >Multimedia Kits</option>			</select>
		</span>
		<input type="search" class="form-control" placeholder="Search &hellip;" value="" name="s" />
		<span class="input-group-btn">
			<button class="btn btn-default"><span class="glyphicon newsroomicons-search"></span></button>
		</span>
	</div>

</form>
						</div>
						<div class="collapse navbar-collapse" id="topnav-menu">
							<ul id="menu-main-top-nav" class="nav navbar-nav"><li id="menu-item-12772" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12772"><a href="http://dclnews.com/releases/">Releases</a></li>
<li id="menu-item-12773" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12773"><a href="http://dclnews.com/photos/">Photos</a></li>
<li id="menu-item-12774" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12774"><a href="http://dclnews.com/videos/">Videos</a></li>
<li id="menu-item-10186" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10186"><a href="http://dclnews.com/fact-sheets/">Fact Sheets</a></li>
<li id="menu-item-12776" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12776"><a href="http://dclnews.com/multimedia-kits/">Multimedia Kits</a></li>
<li id="menu-item-12777" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12777"><a href="http://dclnews.com/contact-us/">Contact Us</a></li>
</ul>						</div>

					</div>
				</nav>
			</div>
			<img width="270" height="56" src="http://static.dclnews.com/wp-content/uploads/sites/4/2014/10/dclnews_logo.png" class="visible-print" alt="dclnews_logo" />		</header>
		<div id="theme-banner">
			<div class="visible-xs">
							</div>
			<div class="visible-sm">
							</div>
		</div>	<section class="content">
		<div class="row carousel-row">
	<div class="feature-slideshow">
		<div class="flexslider flexslider-main">
			<ul class="slides">
										<li class="slide">
							<a href="http://dclnews.com/2015/03/24/disney-cruise-line-to-sail-first-british-isles-itinerary-in-2016-2/">
								<img width="1180" height="500" src="http://static.dclnews.com/wp-content/uploads/sites/4/2015/03/IMG_0389_adj-1180x500.jpg" class="attachment-1180x500 wp-post-image" alt="In summer 2016, Disney Cruise Line will debut a new 12-night itinerary to the British Isles, including new ports-of-call in England, Ireland, France and Scotland.where centuries-old castles set the scene for adventures ashore. (Disney Cruise Line)" />								<div class="caption">
									<h3>Disney Cruise Line to Sail First British Isles Itinerary in 2016</h3>
									<p></p>
								</div>
							</a>
						</li>
										<li class="slide">
							<a href="http://dclnews.com/2015/02/11/disney-cruise-line-introduces-star-wars-day-at-sea-on-select-disney-fantasy-sailings/">
								<img width="1180" height="500" src="http://static.dclnews.com/wp-content/uploads/sites/4/2015/02/1012ZW_0580MS-1180x500.jpg" class="attachment-1180x500 wp-post-image" alt="In 2016, Disney Cruise Line guests can experience the legendary adventures and iconic characters from the Star Wars saga for the first time aboard a Disney Cruise Line ship in a brand-new, day-long celebration during eight special sailings: .Star Wars Day at Sea.. The event combines the power of the Force, the magic of Disney and the excitement of cruising for an out-of-this-galaxy experience unlike any other. (Matt Stroshane, photographer)" />								<div class="caption">
									<h3>Disney Cruise Line Introduces Star Wars Day at Sea on Select Disney Fantasy Sailings</h3>
									<p></p>
								</div>
							</a>
						</li>
										<li class="slide">
							<a href="http://dclnews.com/2015/01/22/land-of-frozen-coming-to-disney-cruise-line-this-summer/">
								<img width="1180" height="500" src="http://static.dclnews.com/wp-content/uploads/sites/4/2015/01/0112ZV_0278MS-1180x500.jpg" class="attachment-1180x500 wp-post-image" alt="Beginning in summer 2015, Disney Cruise Line guests can immerse themselves in the animated hit .Frozen. with brand-new experiences inspired by the film, including a spectacular deck party, a three-song stage show production number, new character meet and greets, storybook adventures ashore and more. (Matt Stroshane, photographer)" />								<div class="caption">
									<h3>Land of &#8220;Frozen&#8221; Coming to Disney Cruise Line This Summer</h3>
									<p></p>
								</div>
							</a>
						</li>
										<li class="slide">
							<a href="http://dclnews.com/2014/10/28/disney-cruise-line-reveals-ports-and-itineraries-for-early-2016/">
								<img width="1180" height="500" src="http://static.dclnews.com/wp-content/uploads/sites/4/2014/10/0314ZY_0796DZ-1180x500.jpg" class="attachment-1180x500 wp-post-image" alt="The Disney Fantasy continues the Disney Cruise Line tradition of blending the elegant grace of early 20th century transatlantic ocean liners with contemporary design to create one of the most stylish and spectacular cruise ships afloat. The Disney Fantasy offers modern features, new innovations and unmistakable Disney touches. (Diana Zalucky, photographer)" />								<div class="caption">
									<h3>Disney Cruise Line Reveals Ports and Itineraries for Early 2016</h3>
									<p></p>
								</div>
							</a>
						</li>
										<li class="slide">
							<a href="http://dclnews.com/2014/05/19/disney-cruise-line-returning-to-hawaii-west-coast-and-galveston-in-2015/">
								<img width="1180" height="500" src="http://static.dclnews.com/wp-content/uploads/sites/4/2014/05/0506ZY_0309KP-1180x500.jpg" class="attachment-1180x500 wp-post-image" alt="As part of Disney Cruise Line 2015 fall itineraries, the Disney Wonder will sail to Honolulu on the stunning coast of Oahu, Hawaii. (Kent Phillips, photographer)" />								<div class="caption">
									<h3>Disney Cruise Line Returning to Hawaii, West Coast and Galveston in 2015</h3>
									<p></p>
								</div>
							</a>
						</li>
							</ul>
		</div>
	</div>
</div>
	<div class="headlines slideshow-container">
		<h2>News Headlines <small><a href="http://dclnews.com/releases/">See All</a></small></h2>
		<div class="flexslider flexslider-news">
			<ul class="slides">
									<li class="slide">
						<div class="post-13120 post type-post status-publish format-standard has-post-thumbnail hentry category-port-adventures card">
	<a href="http://dclnews.com/2015/02/18/port-adventures-immerse-disney-cruise-line-guests-in-adventure-and-culture-on-2015-norwegian-fjords-sailings/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2015/02/0129ZV_0142DR-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="In a Port Adventure like only Disney can do, Anna and Elsa join a traditional Norwegian summer celebration for their first appearance in the land that inspired their story. As part of the Disney Cruise Line Northern European summer season, the Disney Magic sails to Åesund, Norway. (Concept photo: David Roark, photographer)" />		</div>
		<div class="details">
			<h3>Port Adventures Immerse Disney Cruise Line Guests in Adventure and Culture on&hellip;</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/port-adventures/" rel="category tag">Port Adventures</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-12945 post type-post status-publish format-standard has-post-thumbnail hentry category-dining-adults card">
	<a href="http://dclnews.com/2015/01/08/disney-cruise-line-expands-collection-of-premium-culinary-offerings-at-remy/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2015/01/1215ZW_0073MS-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="Petites Assiettes de Remy, or Small Plates of Remy, is a culinary journey at Remy (Disney Fantasy/Disney Dream) for adult guests to travel throughout the restaurant to sample small plates of gourmet cuisine and fine wine. The Remy Executive Chef serves as an epicurean guide for this progressive tasting experience. (Matt Stroshane, photographer)" />		</div>
		<div class="details">
			<h3>Disney Cruise Line Expands Collection of Premium Culinary Offerings at Remy</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/dining-adults/" rel="category tag">Dining - Adults</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-11473 post type-post status-publish format-standard has-post-thumbnail hentry category-itineraries card">
	<a href="http://dclnews.com/2014/03/25/disney-cruise-line-charts-new-course-for-norway-in-2015/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2014/03/DSC_7981-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="Norwegian Fjords" />		</div>
		<div class="details">
			<h3>Disney Cruise Line Charts a New Course for Norway in 2015</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/itineraries/" rel="category tag">Itineraries</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-12998 post type-post status-publish format-standard has-post-thumbnail hentry category-castaway-cay card">
	<a href="http://dclnews.com/2015/01/22/inaugural-castaway-cay-challenge-5k-race-on-disney-cruise-line-private-island/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2015/01/0114ZV_0375MS-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="CASTAWAY CAY, Bahamas (Jan. 14, 2015) . More than 700 runners cruised over the finish line during the inaugural Castaway Cay Challenge, a brand-new 5K event hosted by runDisney and Disney Cruise Line on Disney.s private island in the Bahamas, Castaway Cay. The event was the first of its kind to be held amid the lush palm trees and turquoise waters of a private island paradise, making it the ultimate tropical fun run in the sun. Participants earned an exclusive inaugural race medal to commemorate the event, which came on the heels of the annual Walt Disney World Marathon Weekend. The experience was made complete during a four-night cruise aboard the Disney Dream which sailed from Port Canaveral, Fla., and featured special activities, speakers and merchandise designed exclusively for the Castaway Cay Challenge. (Matt Stroshane, photographer)" />		</div>
		<div class="details">
			<h3>Inaugural Castaway Cay Challenge 5K Race on Disney Cruise Line Private Island</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/castaway-cay/" rel="category tag">Castaway Cay</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-12844 post type-post status-publish format-standard has-post-thumbnail hentry category-awards card">
	<a href="http://dclnews.com/2014/10/21/conde-nast-traveler-readers-name-disney-cruise-line-no-1-in-the-2014-readers-choice-awards/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/007_0109zz_1857kp_71691_orig-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="Disney Fantasy and Disney Dream at Night" />		</div>
		<div class="details">
			<h3>Condéast Traveler Readers Name Disney Cruise Line No. 1 in the&hellip;</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/general/awards/" rel="category tag">Awards</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5369 post type-post status-publish format-standard has-post-thumbnail hentry category-castaway-cay category-families category-headlines-family-fun card">
	<a href="http://dclnews.com/2014/01/01/fun-in-and-out-of-the-sun-at-castaway-cay-disneys-private-island-paradise/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0103zz_0093dz_jpg_24622_orig-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="Guests at Castaway Cay" />		</div>
		<div class="details">
			<h3>Fun In (And Out Of!) the Sun at Castaway Cay, Disney&#8217;s Private&hellip;</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/castaway-cay/" rel="category tag">Castaway Cay</a>, <a href="http://dclnews.com/releases/categories/families/" rel="category tag">Families</a>, <a href="http://dclnews.com/releases/categories/headlines-family-fun/" rel="category tag">Headlines . Family Fun</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5361 post type-post status-publish format-standard has-post-thumbnail hentry category-adults category-disney-dream category-disney-fantasy category-disney-magic category-disney-wonder category-headlines-adults card">
	<a href="http://dclnews.com/2014/01/01/adults-escape-with-indulgences-on-disney-cruise-line/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0409zz_0490ms_jpg_47026_orig-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="Quiet Cove Pool" />		</div>
		<div class="details">
			<h3>Adults Escape with Indulgences on Disney Cruise Line</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/adults/" rel="category tag">Adults</a>, <a href="http://dclnews.com/releases/categories/disney-dream/" rel="category tag">Disney Dream</a>, <a href="http://dclnews.com/releases/categories/disney-fantasy/" rel="category tag">Disney Fantasy</a>, <a href="http://dclnews.com/releases/categories/disney-magic/" rel="category tag">Disney Magic</a>, <a href="http://dclnews.com/releases/categories/disney-wonder/" rel="category tag">Disney Wonder</a>, <a href="http://dclnews.com/releases/categories/headlines-adults/" rel="category tag">Headlines . Adults</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5271 post type-post status-publish format-standard has-post-thumbnail hentry category-disney-magic category-disney-wonder category-families category-headlines-family-fun card">
	<a href="http://dclnews.com/2014/01/01/families-have-fun-and-make-memories-together-aboard-disney-cruise-line/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0108zz_1107kp_68433_orig-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="&quot;Buccaneer Blast&quot; Fireworks at Sea" />		</div>
		<div class="details">
			<h3>Families Have Fun and Make Memories Together Aboard Disney Cruise Line</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/disney-magic/" rel="category tag">Disney Magic</a>, <a href="http://dclnews.com/releases/categories/disney-wonder/" rel="category tag">Disney Wonder</a>, <a href="http://dclnews.com/releases/categories/families/" rel="category tag">Families</a>, <a href="http://dclnews.com/releases/categories/headlines-family-fun/" rel="category tag">Headlines . Family Fun</a></p>
					</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5346 post type-post status-publish format-standard has-post-thumbnail hentry category-children category-disney-dream category-disney-fantasy category-disney-magic category-disney-wonder category-headlines-youth category-teens-tweens card">
	<a href="http://dclnews.com/2014/01/01/disney-cruise-line-offers-immersive-experiences-and-adventurous-activities-for-kids-of-all-ages/">
		<div class="thumb">
			<img width="240" height="180" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/1226bz_1604dz_jpg_54049_orig-240x180.jpg" class="attachment-thumb-240-180 wp-post-image" alt="Disney&#039;s Oceaneer Lab - Magic PlayFloor" />		</div>
		<div class="details">
			<h3>Disney Cruise Line Offers Immersive Experiences and Adventurous Activities for Kids of&hellip;</h3>
										<p>Posted in <a href="http://dclnews.com/releases/categories/children/" rel="category tag">Children</a>, <a href="http://dclnews.com/releases/categories/disney-dream/" rel="category tag">Disney Dream</a>, <a href="http://dclnews.com/releases/categories/disney-fantasy/" rel="category tag">Disney Fantasy</a>, <a href="http://dclnews.com/releases/categories/disney-magic/" rel="category tag">Disney Magic</a>, <a href="http://dclnews.com/releases/categories/disney-wonder/" rel="category tag">Disney Wonder</a>, <a href="http://dclnews.com/releases/categories/headlines-youth/" rel="category tag">Headlines . Youth</a>, <a href="http://dclnews.com/releases/categories/teens-tweens/" rel="category tag">Teens &amp; Tweens</a></p>
					</div>
	</a>
</div>					</li>
							</ul>
		</div>
	</div>
			<div class="row">
				<div class="col-md-10">
						<div class="featured-photos slideshow-container">
		<h2>Featured Photos <small><a href="http://dclnews.com/photos/">All Photos</a></small></h2>
		<div class="flexslider flexslider-home-featured">
			<ul class="slides">
									<li class="slide">
						<div class="post-5746 attachment type-attachment status-inherit hentry card">
	<a href="http://dclnews.com/photos/2011/08/09/disney-dream-at-castaway-cay-5">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0109zz_1392dr_49954_orig-1-140x105.jpg" class="attachment-thumb-140-105" alt="Disney Dream at Castaway Cay" />		</div>
		<div class="details">
			<h3>Disney Dream at Castaway Cay</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5558 attachment type-attachment status-inherit hentry category-disney-dream category-disney-fantasy category-entertainment category-families card">
	<a href="http://dclnews.com/photos/2010/08/16/buccaneer-blast-fireworks-at-sea-on-the-disney-dream-2">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/1226bz_9024ms_jpg_08573_orig-140x105.jpg" class="attachment-thumb-140-105" alt="&quot;Buccaneer Blast!&quot; Fireworks at Sea" />		</div>
		<div class="details">
			<h3>&#8220;Buccaneer Blast!&#8221; Fireworks at Sea</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5838 attachment type-attachment status-inherit hentry category-entertainment card">
	<a href="http://dclnews.com/photos/2010/08/16/the-golden-mickeys-2">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0907ax_1294gd_8d_orig-140x105.jpg" class="attachment-thumb-140-105" alt="The Golden Mickeys" />		</div>
		<div class="details">
			<h3>The Golden Mickeys</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-12661 attachment type-attachment status-inherit hentry card">
	<a href="http://dclnews.com/photos/2014/08/04/palo-12">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2014/08/Palo-140x105.jpg" class="attachment-thumb-140-105" alt="Breathtaking ocean views, rich decor and epicurean excellence create the setting for a romantic escape in a redesigned version of Palo, the Disney Cruise Line signature specialty restaurant, on the Disney Fantasy and Disney Dream. Palo serves superlative modern Italian cuisine in an upscale atmosphere reserved exclusively for adults. (Disney)" />		</div>
		<div class="details">
			<h3>Palo</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5636 attachment type-attachment status-inherit hentry category-disney-dream category-disney-fantasy category-entertainment category-families card">
	<a href="http://dclnews.com/photos/2011/08/03/sailing-away-deck-party-4">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0111zz_0291dz_24540_orig-140x105.jpg" class="attachment-thumb-140-105" alt="&quot;Sailing Away&quot; Deck Party" />		</div>
		<div class="details">
			<h3>&#8220;Sailing Away&#8221; Deck Party</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5541 attachment type-attachment status-inherit hentry card">
	<a href="http://dclnews.com/photos/2011/08/09/aquaduck-water-coaster-on-the-disney-dream-4">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0116zz_0890kp_43323_orig-140x105.jpg" class="attachment-thumb-140-105" alt="AquaDuck Water Coaster" />		</div>
		<div class="details">
			<h3>AquaDuck Water Coaster</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5594 attachment type-attachment status-inherit hentry category-dining-family category-disney-dream card">
	<a href="http://dclnews.com/photos/2011/08/09/animators-palate-undersea-magic-show-2">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/1227bz_0182pm_43887_orig1-140x105.jpg" class="attachment-thumb-140-105" alt="Animator&#039;s Palate Undersea Magic Show" />		</div>
		<div class="details">
			<h3>Animator&#8217;s Palate Undersea Magic Show</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-11676 attachment type-attachment status-inherit hentry card">
	<a href="http://dclnews.com/photos/2014/01/23/marvels-avengers-academy-captain-america-9">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2014/01/1123ZX_1842MS-140x105.jpg" class="attachment-thumb-140-105" alt="On the Disney Magic, super hero &quot;recruits&quot; get a special visit from Captain America while training at Marvel&#039;s Avengers Academy in Disney&#039;s Oceaneer Club. Marvel&#039;s Avengers Academy invites young crime-fighters into a high-tech command post used by The Avengers for special missions and operations training. (Matt Stroshane, photographer)" />		</div>
		<div class="details">
			<h3>Marvel&#8217;s Avengers Academy &#8211; Captain America</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-7563 attachment type-attachment status-inherit hentry category-characters category-children category-disney-fantasy card">
	<a href="http://dclnews.com/photos/2012/02/29/little-mermaid-meets-a-little-cruiser-2">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2012/02/0217ZY_0195MS-140x105.jpg" class="attachment-thumb-140-105" alt="Little Mermaid Meets a Little Cruiser" />		</div>
		<div class="details">
			<h3>Little Mermaid Meets a Little Cruiser</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-5723 attachment type-attachment status-inherit hentry category-castaway-cay category-characters category-children card">
	<a href="http://dclnews.com/photos/2010/08/11/mickey-mouse-on-castaway-cay-2">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/0115zz_0019kp_04253_orig-1-140x105.jpg" class="attachment-thumb-140-105" alt="Mickey Mouse on Castaway Cay" />		</div>
		<div class="details">
			<h3>Mickey Mouse on Castaway Cay</h3>
								</div>
	</a>
</div>					</li>
							</ul>
		</div>
	</div>
	<div class="featured-videos slideshow-container">
		<h2>Featured Videos <small><a href="http://dclnews.com/videos/">All Videos</a></small></h2>
		<div class="flexslider flexslider-home-featured">
			<ul class="slides">
									<li class="slide">
						<div class="post-13150 video type-video status-publish has-post-thumbnail hentry category-itineraries card">
	<a href="http://dclnews.com/videos/2015/03/03/b-roll-2015-norway-itineraries/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2015/03/thumbnail_2_471365d6_v1-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="B-Roll: 2015 Norway Itineraries" />		</div>
		<div class="details">
			<h3>B-Roll: 2015 Norway Itineraries</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-9854 video type-video status-publish has-post-thumbnail hentry category-disney-fantasy category-ship-exteriors card">
	<a href="http://dclnews.com/videos/2012/12/28/df-aerial/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2012/12/thumbnail_2_6765ec2e_v2-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="Disney Fantasy - Aerials" />		</div>
		<div class="details">
			<h3>Disney Fantasy &#8211; Aerials</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-6797 video type-video status-publish has-post-thumbnail hentry category-castaway-cay card">
	<a href="http://dclnews.com/videos/2011/08/22/castaway-cay-b-roll/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2014/03/thumbnail_2_8748d05d_v1-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="B-Roll - Castaway Cay" />		</div>
		<div class="details">
			<h3>B-Roll: Castaway Cay</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-11415 video type-video status-publish has-post-thumbnail hentry category-disney-magic category-families card">
	<a href="http://dclnews.com/videos/2013/10/31/disney-magic-atrium-lobby/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2013/10/thumbnail_2_c5efe459_v1-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="Disney Magic - Atrium Lobby" />		</div>
		<div class="details">
			<h3>Disney Magic &#8211; Atrium Lobby</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-8102 video type-video status-publish has-post-thumbnail hentry category-itineraries card">
	<a href="http://dclnews.com/videos/2012/06/27/b-roll-alaska-aerials/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2012/06/thumbnail_2_1a7959da_v1-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="B-Roll - Alaska Aerials" />		</div>
		<div class="details">
			<h3>B-Roll: Alaska Aerials</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-9874 video type-video status-publish has-post-thumbnail hentry category-disney-fantasy category-ship-interiors card">
	<a href="http://dclnews.com/videos/2012/12/28/disney-fantasy-atrium-lobby/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2012/12/thumbnail_2_be6e27fe_v2-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="Disney Fantasy - Atrium Lobby" />		</div>
		<div class="details">
			<h3>Disney Fantasy &#8211; Atrium Lobby</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-9527 video type-video status-publish has-post-thumbnail hentry category-disney-wonder card">
	<a href="http://dclnews.com/videos/2012/11/20/disney-wonder/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2012/11/thumbnail_2_39d23198_v1-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="Disney Wonder" />		</div>
		<div class="details">
			<h3>B-Roll: Disney Wonder Compilation</h3>
								</div>
	</a>
</div>					</li>
									<li class="slide">
						<div class="post-6738 video type-video status-publish has-post-thumbnail hentry category-disney-dream category-ship-interiors card">
	<a href="http://dclnews.com/videos/2011/08/22/disney-dream-atrium-lobby/">
		<div class="thumb">
			<img width="140" height="105" src="http://static.dclnews.com/wp-content/uploads/sites/4/2011/08/thumbnail_2_17dfb7d3_v4-140x105.jpg" class="attachment-thumb-140-105 wp-post-image" alt="Disney Dream - Atrium Lobby" />		</div>
		<div class="details">
			<h3>Disney Dream &#8211; Atrium Lobby</h3>
								</div>
	</a>
</div>					</li>
							</ul>
		</div>
	</div>
				</div>
				<div class="col-md-5">
					<aside class="sidebar" role="complementary" id="secondary">
	<div id="linkcat-796" class="show-arrows widget widget_links_list"><h1 class="widget-title">Related Links</h1>
	<ul class='xoxo blogroll'>
<li><a href="http://disneycruiselinepublicaffairs.com" target="_blank"><span>About Disney Cruise Line</span></a></li>
<li><a href="http://dclnews.com/categories/itineraries/"><span>Itineraries</span></a></li>
<li><a href="http://dclnews.com/guidelines-for-travel-guides/"><span>Guidelines for Travel Guidebooks</span></a></li>
<li><a href="http://disneyparks.disney.go.com/blog/category/disney-cruise-line/"><span>Disney Parks Blog</span></a></li>

	</ul>
</div>
</aside><!-- #secondary -->				</div>
			</div>
			</section>

<footer role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-15 text-right">
				<h2 class="parks-logo">Disney Parks</h2>
			</div>
			<div class="col-md-2 col-sm-3 col-xs-7 col-md-offset-2 col-sm-offset-1"><h3>Translated News</h3>
<ul class="sub-menu">
	<li id="menu-item-12779" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12779"><a href="/releases/?lang=es">Españ/a></li>
	<li id="menu-item-12780" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12780"><a href="/releases/?lang=pt">Portuguê/a></li>
</ul>
</div>
<div class="col-md-2 col-sm-3 col-xs-8"><h3>Other Newsrooms</h3>
<ul class="sub-menu">
	<li id="menu-item-12782" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12782"><a href="http://disneylandnews.com">Disneyland Resort</a></li>
	<li id="menu-item-12783" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12783"><a href="http://wdwnews.com">Walt Disney World</a></li>
	<li id="menu-item-12924" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12924"><a href="http://disneyaulaninews.com">Disney Aulani News</a></li>
	<li id="menu-item-12784" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12784"><a href="http://disneysportsnews.com">Disney Sports News</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-3 col-xs-7"><h3>Feed Center</h3>
<ul class="sub-menu">
	<li id="menu-item-12786" class="newsroomicons-rss menu-item menu-item-type-custom menu-item-object-custom menu-item-12786"><a target="_blank" href="/releases/feed/">Latest News</a></li>
	<li id="menu-item-12787" class="newsroomicons-rss menu-item menu-item-type-custom menu-item-object-custom menu-item-12787"><a target="_blank" href="/photos/feed/">Photos</a></li>
	<li id="menu-item-12788" class="newsroomicons-rss menu-item menu-item-type-custom menu-item-object-custom menu-item-12788"><a target="_blank" href="http://feeds.feedburner.com/disneyparks">Disney Parks Blog</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-3 col-xs-7"><h3>Connect</h3>
<ul class="sub-menu">
	<li id="menu-item-12791" class="newsroomicons-twitter menu-item menu-item-type-custom menu-item-object-custom menu-item-12791"><a target="_blank" href="http://twitter.com/disneycruise">@DisneyCruise</a></li>
	<li id="menu-item-12792" class="newsroomicons-twitter menu-item menu-item-type-custom menu-item-object-custom menu-item-12792"><a href="http://twitter.com/disneyparks">@DisneyParks</a></li>
	<li id="menu-item-12793" class="newsroomicons-facebook menu-item menu-item-type-custom menu-item-object-custom menu-item-12793"><a href="http://facebook.com/disneycruiseline">Disney Cruise Line</a></li>
	<li id="menu-item-12794" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12794"><a href="http://disneyparks.disney.go.com/blog/category/disney-cruise-line/">Disney Parks Blog</a></li>
</ul>
</div>
		</div>
		<div class="text-center legal">
			<ul><li id="menu-item-12796" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12796"><a target="_blank" href="http://disneytermsofuse.com/">Terms of Use</a></li>
<li id="menu-item-12795" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12795"><a target="_blank" href="http://dclnews.com/supplemental-terms-of-use/">Supplemental Terms of Use</a></li>
<li id="menu-item-12797" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12797"><a target="_blank" href="http://corporate.disney.go.com/corporate/pp.html">Privacy Policy/Your California Privacy Rights</a></li>
<li id="menu-item-12798" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12798"><a target="_blank" href="http://disney.go.com/guestservices/legalnotices?ppLink=pp_wdig">Legal Notices</a></li>
<li id="menu-item-12799" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12799"><a target="_blank" href="http://disney.go.com/sitemap/">Site Map</a></li>
</ul>			<p>&copy; Disney. All Rights Reserved</p>
		</div>
	</div>
</footer>
<script type='text/javascript' src='http://static.dclnews.com/wp-content/themes/disneynewsroom/js/libs.min.js?ver=20150326164127'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var newsroom = {"stylesheet_dir":"http:\/\/dclnews.com\/wp-content\/themes\/disneynewsroom"};
/* ]]> */
</script>
<script type='text/javascript' src='http://static.dclnews.com/wp-content/themes/disneynewsroom/js/main.min.js?ver=20150326164127'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var downloadManager = {"ajaxurl":"http:\/\/dclnews.com\/wp-admin\/admin-ajax.php","ajaxnonce":"f11619f6e2","folderurl":"http:\/\/dclnews.com\/media-folder"};
/* ]]> */
</script>
<script type='text/javascript' src='http://static.dclnews.com/wp-content/themes/disneynewsroom/includes/downloads/js/download-manager.js?ver=4.1.1'></script>
			<script type="text/javascript">var switchTo5x=true;</script>
			<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
			<script type="text/javascript">stLight.options({publisher: "ur-3d66a0cd-212b-f281-fa80-f57d3c96d348", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
						<script type="text/javascript">
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-22192777-1', 'disneyparksnews.com');
					ga('create', 'UA-22192777-4', 'dclnews.com', {'name': 'rollup'});
					ga('require', 'displayfeatures');
					ga('send', 'pageview');
					ga('rollup.send', 'pageview');
				</script>
			<script type="text/javascript">// <![CDATA[
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
// ]]&gt;</script>

<?php get_footer(); ?>
