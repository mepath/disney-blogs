

<!doctype html>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<title>Disney Family | Recipes, Crafts and Activities</title>

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- mobile meta -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons -->
        <link rel="shortcut icon" href="//a.dilcdn.com/a/favicon-5e2244c13558.ico">
		
        <link rel="pingback" href="">


        <script type="text/javascript" src="//cdn.optimizely.com/js/2485060466.js"></script>
<!-- This site is optimized with the Yoast WordPress SEO plugin v1.7.4 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="robots" content="noodp,noydir"/>
<link rel="canonical" href="http://family.disney.com" />
<meta name="msvalidate.01" content="0A95B63CDADDB26AD590E60BDAB5728B" />
<meta name="google-site-verification" content="7L15QTSMVCNlSg5mmjaejZG6jKIvhKEDjGRN1W9JJ6w" />
<script type="application/ld+json">{ "@context": "http://schema.org", "@type": "WebSite", "url": "http://family.disney.com/", "potentialAction": { "@type": "SearchAction", "target": "http://family.disney.com/?s={search_term}", "query-input": "required name=search_term" } }</script>
<!-- / Yoast WordPress SEO plugin. -->

<link rel="dns-prefetch" href="//connect.facebook.net" />
<link rel='stylesheet' id='yarppWidgetCss-css'  href='http://a.dilcdn.com/bl/wp-content/plugins/yet-another-related-posts-plugin/style/widget.css?ver=4.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='swt-stylesheet-css'  href='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/css/main.css?ver=1.23.0' type='text/css' media='all' />
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/js/vendor/modernizr.js?ver=2.5.3'></script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>

<style>
.tm-quiz-container .question-background
{ background-color: #1b2a54 !important; }

.tm-quiz-container .image-fill-color
{ background-color: rgba(227, 184, 58, .3); !important; }

.tm-quiz-container .text-fill-color
{ background-color: #e3b83a !important; }

.tm-quiz-container .tmqz-poll-highlight
{ -webkit-box-shadow: 0 0 0 4px #fff291 !important ; box-shadow: 0 0 0 4px #fff291 !important ; }

.tm-quiz-container .text-percent .checkmark:after
{ border: solid #e3b83a !important; border-width: 0px 6px 6px 0px !important; }

.tm-quiz-container .option-text-background
{ background-color: #f3f7f9 !important; }
</style><meta property="og:image" content="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/08/df_deafult_og.png" />
<meta property="og:site_name" content="Disney Family" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />
<meta property="fb:app_id" content="434027466688626" />
<meta property="og:title" content="Disney Family | Recipes, Crafts and Activities" />
<meta property="og:description" content="Discover Disney-inspired recipes, crafts and activities for kids on Disney Family." />
<meta property="og:url" content="http://family.disney.com" />
        

        
<script type='text/javascript'>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
		var gads = document.createElement('script');
		gads.async = true;
		gads.type = 'text/javascript';
		var useSSL = 'https:' == document.location.protocol;
		gads.src = (useSSL ? 'https:' : 'http:') +
		'//www.googletagservices.com/tag/js/gpt.js';
		var node = document.getElementsByTagName('script')[0];
		node.parentNode.insertBefore(gads, node);
	})();
</script>

<script type='text/javascript'>

	(function() {
		var gads = document.createElement("script");
		gads.async = true;
		gads.type = "text/javascript";
		var useSSL = "https:" == document.location.protocol;
		gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt_mobile.js";
		var node =document.getElementsByTagName("script")[0];
		node.parentNode.insertBefore(gads, node);
	})();

</script>

<script type='text/javascript'>

	googletag.cmd.push(function() {
        
		// 728px wide and above
		var leaderSizeMapping = googletag.sizeMapping().
		addSize([1024, 104], [7, 7]).
		addSize([728, 104], [728, 90]).
		addSize([0, 0], [320, 50]).
		build();
		
		// 769px wide and above will print rectangle 1 on load
		var rect1SizeMapping = googletag.sizeMapping().
		addSize([1024, 264], [300, 250]).
		addSize([0, 0], [7,7]).
		build();

		googletag.pubads().enableAsyncRendering();

		googletag.defineSlot('/7046/family/homepage', [728,90], 'div-gpt-ad-leaderboard1').
		defineSizeMapping(leaderSizeMapping).
		addService(googletag.pubads()).
		setTargeting('position', ['atf']);
		
		googletag.defineSlot('/7046/family/homepage', [300, 250], 'div-gpt-ad-rectangle1').
		defineSizeMapping(rect1SizeMapping).
		addService(googletag.pubads()).
		setTargeting('position', ['atf']);
		
		googletag.pubads().setTargeting('contentType', ['homepage']);
		googletag.pubads().setTargeting('contentId', ['0']);
		googletag.pubads().setTargeting('siteSection', ['homepage']);
		googletag.pubads().setTargeting('pageName', ['homepage']);
		googletag.pubads().setTargeting('unit', ['dol']);
		googletag.pubads().collapseEmptyDivs();
		googletag.enableServices();
		
	});

</script>
	</head>
    
	<body class="home blog">

        <div id="goc-body" class="chrome">
                           
            <div id="goc" style="height:40px">
  <script type="text/javascript">
    var GOC = { opts: {
      bg : "dark"
    }, queue: [ ] };
    (function (d) {
      var s = d.createElement("script");
      s.async = true;
      s.src = "//a.dilcdn.com/g/us/family/responsive.js";
      d.getElementsByTagName("head")[0].appendChild(s);
    })(document);
  </script>
  <noscript>
    <iframe src="//a.dilcdn.com/g/us/family/responsive.html?bg=dark" frameborder="0" style="display:block;width:100%;height:40px"></iframe>
  </noscript>
</div>
<div id="goc-nav-family" class="goc-el">
  <span id="goc-e" role="button" title="Navigate" onclick="GOC.queue.push(['openMenu']);"></span>
  <a id="goc-logo" title="Disney Family" href="/"></a>
  <div class="menu-main-nav-container">
    <ul id="chrome-menu" class="top-nav-chrome">
      <li class="menu-item"><a href="/crafts/">Crafts</a></li>
      <li class="menu-item"><a href="/recipes/">Recipes</a></li>
      <li class="menu-item"><a href="/activities/">Activities</a></li>
    </ul>
  </div>
</div>            <header class="site-header">
    <div id="div-gpt-ad-leaderboard1" class="ad">
        <script type="text/javascript">googletag.cmd.push(function() { googletag.display("div-gpt-ad-leaderboard1"); });</script>
    </div>

    <a href="/" name="&lpos=header/nav&lid=header/nav/img/logo" class="logo">Disney Family</a>

    <ul id="menu-main-nav" class="top-nav"><li id="menu-item-9249" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-9249"><a href="http://family.disney.com/crafts">Crafts</a></li>
<li id="menu-item-9250" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-9250"><a href="http://family.disney.com/recipes">Recipes</a></li>
<li id="menu-item-46805" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-46805"><a href="http://family.disney.com/printables">Printables</a></li>
<li id="menu-item-47195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47195"><a href="http://family.disney.com/spring">Spring</a></li>
<li id="menu-item-47197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47197"><a href="http://family.disney.com/easter">Easter</a></li>
</ul>    
</header>
    		
    		<div class="container">    
<section class="home-section promo">

    <main class="content">

        
            <section class="promo">

                <div class="promo-image box">

                    <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Factivities%2Fdisney-infinity-2-0-activity-pages&media=http://family.disney.com/wp-content/uploads/sites/9/2015/02/INF2_FamilycomHero_Activities_v3.jpg&description=Disney Infinity 2.0 Activities"
                        data-pin-do="none"
                        data-pin-config="none"
                        class="pin" target="_blank">

                        <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />

                    </a>

                    <a name="&lpos=left module-container/module&lid=left module-container/module/promo/Disney Infinity 2.0 Activities"
                        href="http://family.disney.com/activities/disney-infinity-2-0-activity-pages"
                        class="box-content">

                        <div class="box-image lazy"
                            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2015/02/INF2_FamilycomHero_Activities_v3.jpg"
                            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                        </div>

                    </a>

                </div>

                <div class="promo-details activities">

                    <a name="&lpos=left module-container/module&lid=left module-container/module/promo/Disney Infinity 2.0 Activities"
                        href="http://family.disney.com/activities/disney-infinity-2-0-activity-pages"
                        class="promo-title">

                        <h2>Disney Infinity 2.0 Activities</h2>

                    </a>

                    <p>Enjoy these activities inspired by the Toy Box 2.0, where you have endless opportunities to create your own stories and fun filled adventures!</p>

                </div>

            </section>

        
        
            <section class="box-grouping-5">

                
                    <div class="box articles">

                        
                            <div class="sponsor-wrapper">
                                <a name="&lpos=left module-container/sponsor_module&lid=left module-container/sponsor_module/1/Disney's Cinderella-Inspired Party"
                                    href="http://di.sn/6002FQMC"
                                    class="sponsor">

                                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/10/HP_Blue_CMYKC_IMAGE-218x218.jpg" alt="sponsored image">

                                </a>
                            </div>

                        
                        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Farticles%2Fdisneys-cinderella-inspired-party&media=http://family.disney.com/wp-content/uploads/sites/9/2015/03/CinderellaDIYparty_V3_small-1200x1200.jpg&description=Disney%27s+Cinderella-Inspired+Party"
                            data-pin-do="none"
                            data-pin-config="none"
                            class="pin" target="_blank">

                           <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />

                        </a>

                        <a name="&lpos=left module-container/sponsor_module&lid=left module-container/sponsor_module/1/Disney's Cinderella-Inspired Party" href="http://family.disney.com/articles/disneys-cinderella-inspired-party" title="Disney&#039;s Cinderella-Inspired Party" class="box-content">
                            <div class="box-image" style="background-image: url(http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2015/03/CinderellaDIYparty_V3_small-1200x1200.jpg);"></div>
                            <h2>Disney's Cinderella-Inspired Party</h2>
                        </a>

                    </div>

                
                    <div class="box crafts">

                        
                        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Fpiglet-easter-egg&media=http://family.disney.com/wp-content/uploads/sites/9/2014/04/Piglet_Easter_Egg.jpg&description=Piglet+Easter+Egg"
                            data-pin-do="none"
                            data-pin-config="none"
                            class="pin" target="_blank">

                           <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />

                        </a>

                        <a name="&lpos=left module-container/module&lid=left module-container/2/Piglet Easter Egg" href="http://family.disney.com/crafts/piglet-easter-egg" title="Piglet Easter Egg" class="box-content">
                            <div class="box-image" style="background-image: url(http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/04/Piglet_Easter_Egg.jpg);"></div>
                            <h2>Piglet Easter Egg</h2>
                        </a>

                    </div>

                
                    <div class="box recipes">

                        
                        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Frecipes%2Fhoney-bee-mines&media=http://family.disney.com/wp-content/uploads/sites/9/2012/03/Honey-Bees-photo-280x206.jpg&description=Honey+Bee+Mines"
                            data-pin-do="none"
                            data-pin-config="none"
                            class="pin" target="_blank">

                           <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />

                        </a>

                        <a name="&lpos=left module-container/module&lid=left module-container/3/Honey Bee Mines" href="http://family.disney.com/recipes/honey-bee-mines" title="Honey Bee Mines" class="box-content">
                            <div class="box-image" style="background-image: url(http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/Honey-Bees-photo-280x206.jpg);"></div>
                            <h2>Honey Bee Mines</h2>
                        </a>

                    </div>

                
                    <div class="box crafts">

                        
                        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Falice-in-wonderland-golden-afternoon-flowers&media=http://family.disney.com/wp-content/uploads/sites/9/2013/02/alice-in-wonderland-flowers-printable-photo-420x420-fs-IMG_0263.jpg&description=Alice+in+Wonderland+Golden+Afternoon+Flowers"
                            data-pin-do="none"
                            data-pin-config="none"
                            class="pin" target="_blank">

                           <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />

                        </a>

                        <a name="&lpos=left module-container/module&lid=left module-container/4/Alice in Wonderland Golden Afternoon Flowers" href="http://family.disney.com/crafts/alice-in-wonderland-golden-afternoon-flowers" title="Alice in Wonderland Golden Afternoon Flowers" class="box-content">
                            <div class="box-image" style="background-image: url(http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2013/02/alice-in-wonderland-flowers-printable-photo-420x420-fs-IMG_0263.jpg);"></div>
                            <h2>Alice in Wonderland Golden Afternoon Flowers</h2>
                        </a>

                    </div>

                
                    <div class="box crafts">

                        
                        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Fbambi-cutie-stickers&media=http://family.disney.com/wp-content/uploads/sites/9/2012/03/bambi-stickers-printable-photo-420x420-fs-3999.jpg&description=Bambi+Cutie+Stickers"
                            data-pin-do="none"
                            data-pin-config="none"
                            class="pin" target="_blank">

                           <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />

                        </a>

                        <a name="&lpos=left module-container/module&lid=left module-container/5/Bambi Cutie Stickers" href="http://family.disney.com/crafts/bambi-cutie-stickers" title="Bambi Cutie Stickers" class="box-content">
                            <div class="box-image" style="background-image: url(http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/bambi-stickers-printable-photo-420x420-fs-3999.jpg);"></div>
                            <h2>Bambi Cutie Stickers</h2>
                        </a>

                    </div>

                
            </section>

        
    </main>

    <aside class="sidebar">

        <div id="div-gpt-ad-rectangle1" class="ad">
            <script type="text/javascript">googletag.cmd.push(function() { googletag.display("div-gpt-ad-rectangle1"); });</script>
        </div>

        
                <div class="box activities circle">

                    <a name="&lpos=right module-container/module&lid=right module-container/module/1/Mickey &amp; Friends Easter Activity Page" href="http://family.disney.com/activities/mickey-friends-activity-page" title="Mickey &amp; Friends Easter Activity Page">

                        <div class="box-content" >
                            <div class="box-image lazy"
                            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/easter-activity-page-printable-photo-420.jpg"
                            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>

                        <h2>Mickey &amp; Friends Easter Activity Page</h2>

                    </a>

                </div>

            
                <div class="box crafts circle">

                    <a name="&lpos=right module-container/module&lid=right module-container/module/2/Disney Fairy Flower Rings" href="http://family.disney.com/crafts/disney-fairy-flower-rings" title="Disney Fairy Flower Rings">

                        <div class="box-content" >
                            <div class="box-image lazy"
                            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/04/fairy-flower-ring-printable-photo-420x420-fs-img_2649.jpg"
                            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>

                        <h2>Disney Fairy Flower Rings</h2>

                    </a>

                </div>

            
                <div class="box crafts circle">

                    <a name="&lpos=right module-container/module&lid=right module-container/module/3/Butterfly Journal Cover" href="http://family.disney.com/crafts/butterfly-journal-cover" title="Butterfly Journal Cover">

                        <div class="box-content" >
                            <div class="box-image lazy"
                            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/04/butterfly-journal-craft-photo-420x420-clittlefield-G.jpg"
                            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>

                        <h2>Butterfly Journal Cover</h2>

                    </a>

                </div>

            
                <div class="box crafts circle">

                    <a name="&lpos=right module-container/module&lid=right module-container/module/4/Thumper's Easter Pop-up Card" href="http://family.disney.com/crafts/thumpers-easter-pop-up-card" title="Thumper&#039;s Easter Pop-up Card">

                        <div class="box-content" >
                            <div class="box-image lazy"
                            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/04/disney-easter-printables-photo-R-CP-420x420-IMG_5029.jpg"
                            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>

                        <h2>Thumper's Easter Pop-up Card</h2>

                    </a>

                </div>

            
    </aside>

</section>    
    <section class="home-section crafts">
    
    <div class="section-header">
        
        <h2><span class="icon"></span>Crafts</h2>
        <a name="&lpos=crafts_section/header&lid=crafts_section/header/link/view-all" href="/crafts">View All</a>
    
    </div>

    <main class="content">
    
            
                        
                    <section class="box-grouping-3 left">
    
                

	<div class="box crafts">

        <!-- check if sponsored article radio button is selected in post editor -->
        
            <div class="sponsor-wrapper">
                <a name="&lpos=left module-container/sponsor_module&lid=left module-container/sponsor_module/oralb" href="http://di.sn/6005FzOJ" class="sponsor">
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/Crest-OralB4-218x55.jpg" alt="sponsored image">
                </a>
            </div>

        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Fdoc-mcstuffins-winning-grin-pins&media=http://family.disney.com/wp-content/uploads/sites/9/2014/07/winning_grin_pins_social2-1200x1200.jpg&description=Doc+McStuffins+Winning+Grin+Pins"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/sponsor_module&lid=left module-container/sponsor_module/Doc McStuffins Winning Grin Pins" href="http://family.disney.com/crafts/doc-mcstuffins-winning-grin-pins"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/winning_grin_pins_social2-1200x1200.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Doc McStuffins Winning Grin Pins</h2>
        </a>

    </div>



	<div class="box crafts">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Fcinderellas-glass-slipper-craft&media=http://family.disney.com/wp-content/uploads/sites/9/2015/03/Glass-Slipper.jpg&description=Cinderella%27s+Glass+Slipper+Craft"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Cinderella's Glass Slipper Craft" href="http://family.disney.com/crafts/cinderellas-glass-slipper-craft"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2015/03/Glass-Slipper.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Cinderella's Glass Slipper Craft</h2>
        </a>

    </div>



	<div class="box crafts">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Feeyore-easter-egg&media=http://family.disney.com/wp-content/uploads/sites/9/2014/07/Eeyore-Easter-Egg.jpg&description=Eeyore+Easter+Egg"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Eeyore Easter Egg" href="http://family.disney.com/crafts/eeyore-easter-egg"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/Eeyore-Easter-Egg.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Eeyore Easter Egg</h2>
        </a>

    </div>

    
                    </section>
    
                    
                    <section class="box-grouping-2 right">

                

	<div class="box crafts">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Fisabellas-whatcha-doin-diary&media=http://family.disney.com/wp-content/uploads/sites/9/2012/03/isabella-diary-craft-photo-420x420-clittlefield-c.jpg&description=Isabella%27s+%22Whatcha+Doin%27%3F%22+Diary"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Isabella's "Whatcha Doin'?" Diary" href="http://family.disney.com/crafts/isabellas-whatcha-doin-diary"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/isabella-diary-craft-photo-420x420-clittlefield-c.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Isabella's "Whatcha Doin'?" Diary</h2>
        </a>

    </div>



	<div class="box crafts">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Fcrafts%2Fthe-art-of-the-towel-animal-elephant&media=http://family.disney.com/wp-content/uploads/sites/9/2014/07/the-art-of-the-towel-animal-elephant.jpg&description=The+Art+of+The+Towel+Animal%3A+Elephant"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/The Art of The Towel Animal: Elephant" href="http://family.disney.com/crafts/the-art-of-the-towel-animal-elephant"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/the-art-of-the-towel-animal-elephant.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>The Art of The Towel Animal: Elephant</h2>
        </a>

    </div>

    
                    </section>
    
                            
                    
            
    </main>
    
    <aside class="sidebar">
    
         
      
                <div class="box crafts circle">
      
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Frozen Online Party Invitation" href="http://family.disney.com/crafts/frozen-online-party-invitation" title="Frozen Online Party Invitation">
      
                        <div class="box-content">
      
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2015/02/frozen03_1000x1000.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
      
                        </div>
      
                        <h2>Frozen Online Party Invitation</h2>
      
                    </a>
      
                </div>
      
             
      
                <div class="box crafts circle">
      
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Kermit the Frog Cardboard Puppet" href="http://family.disney.com/crafts/kermit-the-frog-cardboard-puppet" title="Kermit the Frog Cardboard Puppet">
      
                        <div class="box-content">
      
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/Kermit-cardboard-puppet420.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
      
                        </div>
      
                        <h2>Kermit the Frog Cardboard Puppet</h2>
      
                    </a>
      
                </div>
      
             
      
                <div class="box crafts circle">
      
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Monsters, Inc. Employee Badge" href="http://family.disney.com/crafts/monsters-inc-employee-badge" title="Monsters, Inc. Employee Badge">
      
                        <div class="box-content">
      
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/11/monsters-inc-badge-printable-photo-420x420-fs-img_9282.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
      
                        </div>
      
                        <h2>Monsters, Inc. Employee Badge</h2>
      
                    </a>
      
                </div>
      
             
      
                <div class="box crafts circle">
      
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Paper Plate Animal from The Muppets" href="http://family.disney.com/crafts/paper-plate-animal-from-the-muppets" title="Paper Plate Animal from The Muppets">
      
                        <div class="box-content">
      
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/03/paper_plate_animal_from_the_muppet_420.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
      
                        </div>
      
                        <h2>Paper Plate Animal from The Muppets</h2>
      
                    </a>
      
                </div>
      
                    
    </aside>
</section>    <section class="home-section recipes">
    
    <div class="section-header">
        <h2><span class="icon"></span>Recipes</h2>
        <a  name="&lpos=recipes_section/header&lid=recipes_section/header/link/view-all"  href="/recipes">View All</a>
    </div>

    <main class="content">
        
                                <section class="box-grouping-3 right">
        
                

	<div class="box recipes">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Frecipes%2Fjakes-veggie-pirate-treasure&media=http://family.disney.com/wp-content/uploads/sites/9/2015/03/disneydishes_jakespirate_2.jpeg&description=Jake%27s+Veggie+Pirate+Treasure"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Jake's Veggie Pirate Treasure" href="http://family.disney.com/recipes/jakes-veggie-pirate-treasure"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2015/03/disneydishes_jakespirate_2.jpeg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Jake's Veggie Pirate Treasure</h2>
        </a>

    </div>



	<div class="box recipes">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Frecipes%2Fevil-queens-villainous-apples&media=http://family.disney.com/wp-content/uploads/sites/9/2014/10/Poison-Apples2-small-1200x1200.jpg&description=Evil+Queen%27s+Villainous+Apples"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Evil Queen's Villainous Apples" href="http://family.disney.com/recipes/evil-queens-villainous-apples"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/10/Poison-Apples2-small-1200x1200.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Evil Queen's Villainous Apples</h2>
        </a>

    </div>



	<div class="box recipes">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Frecipes%2Fpanda-bear-cupcakes&media=http://family.disney.com/wp-content/uploads/sites/9/2012/03/panda-bear-cupcakes-recipe-photo-420x420-cl-003.jpg&description=Panda+Bear+Cupcakes"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Panda Bear Cupcakes" href="http://family.disney.com/recipes/panda-bear-cupcakes"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/panda-bear-cupcakes-recipe-photo-420x420-cl-003.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Panda Bear Cupcakes</h2>
        </a>

    </div>

                    </section>
                        
                    <section class="box-grouping-2 left">

                

	<div class="box recipes">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Frecipes%2Fsleepys-quick-energy-nuggets&media=http://family.disney.com/wp-content/uploads/sites/9/2012/03/sleepys-quick-energy-nuggets-recipe-photo-420x420-cl-003.jpg&description=Sleepy%27s+Quick+Energy+Nuggets"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Sleepy's Quick Energy Nuggets" href="http://family.disney.com/recipes/sleepys-quick-energy-nuggets"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/sleepys-quick-energy-nuggets-recipe-photo-420x420-cl-003.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Sleepy's Quick Energy Nuggets</h2>
        </a>

    </div>



	<div class="box recipes">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Frecipes%2Flittle-mermaid-cake&media=http://family.disney.com/wp-content/uploads/sites/9/2014/07/Ariel-Cake-E-cropped_small_1.jpg&description=Little+Mermaid+Cake"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Little Mermaid Cake" href="http://family.disney.com/recipes/little-mermaid-cake"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/Ariel-Cake-E-cropped_small_1.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Little Mermaid Cake</h2>
        </a>

    </div>

                    </section>
                                        </main>
    <aside class="sidebar">
         
                <div class="box recipes circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Cinderella Bento Box" href="http://family.disney.com/recipes/cinderella-bento-box" title="Cinderella Bento Box">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/princess-bento-recipe-photo-420x420-scarino-4.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>Cinderella Bento Box</h2>
                    </a>
                </div>
             
                <div class="box recipes circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Everblossom Cupcakes" href="http://family.disney.com/recipes/everblossom-cupcakes" title="Everblossom Cupcakes">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/Disney-Fairies-cupcakes-photo-420x420-CL-EverblossomB.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>Everblossom Cupcakes</h2>
                    </a>
                </div>
             
                <div class="box recipes circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Mini Cheese Ball Mice" href="http://family.disney.com/recipes/mini-cheese-ball-mice-2" title="Mini Cheese Ball Mice">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2013/12/mini-cheese-ball-mice-recipe-photo-420x420-cl-002.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>Mini Cheese Ball Mice</h2>
                    </a>
                </div>
             
                <div class="box recipes circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Minnie Mouse Cinnamon Buns with Strawberry Bows" href="http://family.disney.com/recipes/minnie-mouse-cinnamon-buns-with-strawberry-bows" title="Minnie Mouse Cinnamon Buns with Strawberry Bows">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/Minnie-Mouse-Cinnamon-Rolls-with-Strawberry-Bows_0.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>Minnie Mouse Cinnamon Buns with Strawberry Bows</h2>
                    </a>
                </div>
                    
    </aside>
</section>    <section class="home-section activities">
    <div class="section-header">
        <h2><span class="icon"></span>Activities</h2>
        <a name="&lpos=activities_section/header&lid=activities_section/header/link/view-all" href="/activities">View All</a>
    </div>

    <main class="content">
                                        <section class="box-grouping-3 left">
                

	<div class="box activities">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Factivities%2Fdumbos-circus-coloring-page&media=http://family.disney.com/wp-content/uploads/sites/9/2012/04/coloring-page-dumbo-printable-photo-420x420-fs-3514-2.jpg&description=Dumbo%27s+Circus+Coloring+Page"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Dumbo's Circus Coloring Page" href="http://family.disney.com/activities/dumbos-circus-coloring-page"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/04/coloring-page-dumbo-printable-photo-420x420-fs-3514-2.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Dumbo's Circus Coloring Page</h2>
        </a>

    </div>



	<div class="box activities">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Factivities%2Fdisney-packing-game&media=http://family.disney.com/wp-content/uploads/sites/9/2014/09/DYDIS-24331-003Z-v05-small-1200x1200.jpg&description=Disney+Packing+Game"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Disney Packing Game" href="http://family.disney.com/activities/disney-packing-game"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/09/DYDIS-24331-003Z-v05-small-1200x1200.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Disney Packing Game</h2>
        </a>

    </div>



	<div class="box activities">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Factivities%2Fdisney-mickeys-typing-adventure-coloring-page&media=http://family.disney.com/wp-content/uploads/sites/9/2015/03/MTA-Coloring-Page_image-1200x1200.jpg&description=Disney+Mickey%27s+Typing+Adventure+Coloring+Page"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Disney Mickey's Typing Adventure Coloring Page" href="http://family.disney.com/activities/disney-mickeys-typing-adventure-coloring-page"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2015/03/MTA-Coloring-Page_image-1200x1200.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Disney Mickey's Typing Adventure Coloring Page</h2>
        </a>

    </div>

                    </section>
                                    <section class="box-grouping-2 right">

                

	<div class="box activities">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Factivities%2Ffrozen-snowflake-maze&media=http://family.disney.com/wp-content/uploads/sites/9/2014/11/frozen-snowflake-maze.png&description=Frozen+Snowflake+Maze"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Frozen Snowflake Maze" href="http://family.disney.com/activities/frozen-snowflake-maze"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/11/frozen-snowflake-maze.png"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Frozen Snowflake Maze</h2>
        </a>

    </div>



	<div class="box activities">

        <!-- check if sponsored article radio button is selected in post editor -->
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Ffamily.disney.com%2Factivities%2Ffairy-godmother-coloring-page&media=http://family.disney.com/wp-content/uploads/sites/9/2014/07/CLA-Fairy-Godmother-Coloring-Page_FDCOM2_11.jpg&description=Fairy+Godmother+Coloring+Page"  data-pin-do="none" data-pin-config="none" class="pin" target="_blank">
            <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/pinterest_btn.png" width="56" height="28" />
        </a>
        <a name="&lpos=left module-container/module&lid=left module-container/Fairy Godmother Coloring Page" href="http://family.disney.com/activities/fairy-godmother-coloring-page"  class="box-content">

        <div class="box-image lazy"
            data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/07/CLA-Fairy-Godmother-Coloring-Page_FDCOM2_11.jpg"
            style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
        </div>
            <h2>Fairy Godmother Coloring Page</h2>
        </a>

    </div>

                    </section>
                                        </main>
    <aside class="sidebar">
         
                <div class="box activities circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Disney Infinity 2.0 Activity Pages" href="http://family.disney.com/activities/disney-infinity-2-0-activity-pages" title="Disney Infinity 2.0 Activity Pages">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/10/disney-infinity-2-activity-pages-FDCOM.png" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>Disney Infinity 2.0 Activity Pages</h2>
                    </a>
                </div>
             
                <div class="box activities circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Mickey &amp; Friends Ice Skating Playset" href="http://family.disney.com/activities/mickey-friends-ice-skating-playset" title="Mickey &amp; Friends Ice Skating Playset">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/mickey-and-friends-ice-pond-playset-winter-printable-photo-420x420-fs-0285.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>Mickey &amp; Friends Ice Skating Playset</h2>
                    </a>
                </div>
             
                <div class="box activities circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/Sleeping Beauty Movie Game" href="http://family.disney.com/activities/sleeping-beauty-movie-game" title="Sleeping Beauty Movie Game">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2014/09/sleeping-beauty-castle-small-1200x1200.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>Sleeping Beauty Movie Game</h2>
                    </a>
                </div>
             
                <div class="box activities circle">
                    <a  name="&lpos=right module-container/module&lid=right module-container/module/The Muppet Theater Playset" href="http://family.disney.com/activities/the-muppet-theater-playset" title="The Muppet Theater Playset">
                        <div class="box-content">
                            <div class="box-image lazy" 
                                data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/muppets-playset-printables-photo-420x420-fs_MG_2479.jpg" 
                                style="background-image: url(http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/images/lazy.png);">
                            </div>
                        </div>
                        <h2>The Muppet Theater Playset</h2>
                    </a>
                </div>
                    
    </aside>
</section>
        </div> <!-- end .container -->
        <footer class="site-footer">
            <section class="site-footer-top">
                <div class="featured crafts">
    <h3>Featured Crafts</h3>
    <ul>
         

            <li>
                
                
                <a name="&lpos=featured-crafts-container/module&lid=featured-crafts-container/module/Butterfly Journal Cover" 
                    href="http://family.disney.com/crafts/butterfly-journal-cover" 
                    title="Butterfly Journal Cover">
                
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/04/butterfly-journal-craft-photo-420x420-clittlefield-G-120x120.jpg">                    
                    <p>Butterfly Journal Cover</p>
                
                </a>

            </li>
        
         

            <li>
                
                
                <a name="&lpos=featured-crafts-container/module&lid=featured-crafts-container/module/Mickey &amp; Friends Cutie Charm Bracelets" 
                    href="http://family.disney.com/crafts/mickey-friends-cutie-charm-bracelets" 
                    title="Mickey &amp; Friends Cutie Charm Bracelets">
                
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/shrinkie-dink-charms-mothers-day-printable-craft-photo-420x420-fs-img_0944-120x120.jpg">                    
                    <p>Mickey &amp; Friends Cutie Charm Bracelets</p>
                
                </a>

            </li>
        
         

            <li>
                
                
                <a name="&lpos=featured-crafts-container/module&lid=featured-crafts-container/module/Nala Cutie Papercraft" 
                    href="http://family.disney.com/crafts/nala-cutie-papercraft" 
                    title="Nala Cutie Papercraft">
                
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/nala-cutie-lion-king-printable-photo-420x420-fs-3842-120x120.jpg">                    
                    <p>Nala Cutie Papercraft</p>
                
                </a>

            </li>
        
            </ul>
</div>                <div class="featured recipes">
    <h3>Featured Recipes</h3>
    <ul>
         
            
            <li>
                
                
                <a name="&lpos=featured-recipes-container/module&lid=featured-recipes-container/module/Honey Lemon.s Honeybee Cupcakes" 
                    href="http://family.disney.com/recipes/honey-lemons-honeybee-cupcakes" 
                    title="Honey Lemon.s Honeybee Cupcakes">
                    
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2015/01/big-hero-6-honeybee-cupcakes-120x120.jpg">                    
                    <p>Honey Lemon.s Honeybee Cupcakes</p>
                
                </a>

            </li>

         
            
            <li>
                
                
                <a name="&lpos=featured-recipes-container/module&lid=featured-recipes-container/module/Tiny Teacakes" 
                    href="http://family.disney.com/recipes/tiny-teacakes" 
                    title="Tiny Teacakes">
                    
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/tiny-teacakes-recipe-photo-420x420-cl-004-120x120.jpg">                    
                    <p>Tiny Teacakes</p>
                
                </a>

            </li>

         
            
            <li>
                
                
                <a name="&lpos=featured-recipes-container/module&lid=featured-recipes-container/module/Baked Tortilla Leaves" 
                    href="http://family.disney.com/recipes/baked-tortilla-leaves" 
                    title="Baked Tortilla Leaves">
                    
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/03/baked-tortilla-leaves-recipe-photo-420x420-cl-004-120x120.jpg">                    
                    <p>Baked Tortilla Leaves</p>
                
                </a>

            </li>

            </ul>
</div>                <div class="featured activities">
    <h3>Featured Activities</h3>
    <ul>
        
            <li>

                                
                <a name="&lpos=featured-activities-container/module&lid=featured-activities-container/module/Wreck-It Ralph Coloring Page" 
                    href="http://family.disney.com/activities/wreck-it-ralph-coloring-page" 
                    title="Wreck-It Ralph Coloring Page">
                
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/09/coloring-page-wreck-it-ralph-printable-photo-420x420-fs-img_0343-2-120x120.jpg">                    
                    <p>Wreck-It Ralph Coloring Page</p>
            
                </a>
            
            </li>
        
        
            <li>

                                
                <a name="&lpos=featured-activities-container/module&lid=featured-activities-container/module/Toy Story Fun Book" 
                    href="http://family.disney.com/activities/toy-story-fun-book" 
                    title="Toy Story Fun Book">
                
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2012/04/toy-story-coloring-page-120x120.jpg">                    
                    <p>Toy Story Fun Book</p>
            
                </a>
            
            </li>
        
        
            <li>

                                
                <a name="&lpos=featured-activities-container/module&lid=featured-activities-container/module/Lilo &amp; Stitch Math Problem Paint by Number Worksheet" 
                    href="http://family.disney.com/activities/lilo-stitch-math-problem-paint-by-number-worksheet" 
                    title="Lilo &amp; Stitch Math Problem Paint by Number Worksheet">
                
                    <img class="lazy" data-original="http://a.dilcdn.com/bl/wp-content/uploads/sites/9/2013/04/disney-teachers-corner-math-paint-by-number-printable-photo-420x420-0413-120x120.jpg">                    
                    <p>Lilo &amp; Stitch Math Problem Paint by Number Worksheet</p>
            
                </a>
            
            </li>
        
            </ul>
</div>            </section>
            <section class="site-footer-bottom">
                <ul class="disney-links">
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/About Disney" target="_blank" href="http://corporate.disney.go.com/index.html?ppLink=pp_wdig">About Disney</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Help Guest Services" target="_blank" href="http://disney.go.com/guestservices/">Help &amp; Guest Services</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Careers" target="_blank" href="http://disneycareers.com/en/default/">Careers</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Contact Us" target="_blank" href="http://disney.go.com/guestservices/">Contact Us</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Starwars" target="_blank" href="http://www.starwars.com/">Star Wars</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Babble" target="_blank" href="http://babble.com/">Babble</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Disney" target="_blank" href="http://www.disney.com/">Disney</a></li>
                </ul>
                <ul class="legal-links">
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Terms of Use" target="_blank" href="http://disneytermsofuse.com">Terms of Use</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Legal Notices" target="_blank" href="http://disney.go.com/guestservices/legalnotices?ppLink=pp_wdig">Legal Notices</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Privacy Policy" target="_blank" href="https://disneyprivacycenter.com">Privacy Policy</a></li>

                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Your California Privacy Rights" target="_blank" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a></li>

                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Children Online Privacy Policy" target="_blank" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children.s Online Privacy Policy</a></li>
                  
                    <li><a name="&lpos=GlobalFooter&lid=GlobalFooter/link/Interest-Based Ads" target="_blank" href="http://disney.go.com/guestservices/disclaimers/redirect?destination=http://preferences.truste.com/2.0/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a></li>
                </ul>
                <div class="copyright">© Disney, All Rights Reserved, Disney Lifestyle</div>
            </section>
        </footer>
        <script language="javascript" src="http://aglobal.go.com/stat/dolWebAnalytics.js"></script>
<script language="javascript">
cto = new CTO();
cto.account = "wdgdolfamily";
cto.categoryCode = "dcom";
cto.siteCode = "family";
cto.brandSegment = "";
cto.buCode = "";
cto.program = "";
cto.breadcrumbs = "homepage";
cto.pageName = "main_1";
cto.postDate = "2014/07/30 11:17:52 PM";
cto.adPageName = "/7046/family/";
cto.adKeywords = "homepage";
cto.pageId = "0";
cto.author = "";
cto.contentTag = "";

cto.track();
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var FB_WP=FB_WP||{};FB_WP.queue={_methods:[],flushed:false,add:function(fn){FB_WP.queue.flushed?fn():FB_WP.queue._methods.push(fn)},flush:function(){for(var fn;fn=FB_WP.queue._methods.shift();){fn()}FB_WP.queue.flushed=true}};window.fbAsyncInit=function(){FB.init({"xfbml":true,"appId":"434027466688626"});if(FB_WP && FB_WP.queue && FB_WP.queue.flush){FB_WP.queue.flush()}}
/* ]]> */
</script>
<script type="text/javascript">(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return}js=d.createElement(s);js.id=id;js.src="http:\/\/connect.facebook.net\/en_US\/all.js";fjs.parentNode.insertBefore(js,fjs)}(document,"script","facebook-jssdk"));</script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/js/foundation.min.js?ver=4.1.1'></script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/js/extend.js?ver=4.1.1'></script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/js/vendor/jquery.dotdotdot.min.js?ver=1.6.14'></script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/js/vendor/jquery.lazyload.min.js?ver=4.1.1'></script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/js/vendor/dis-chrome.js?ver=4.1.1'></script>
<script type='text/javascript' src='http://a.dilcdn.com/bl/wp-content/themes/family-disney-theme/library/js/theme-scripts.js?ver=1.23.0'></script>
<div id="fb-root"></div>                </div>



	</body>
</html> <!-- end page -->
<!-- Performance optimized by W3 Total Cache. Learn more: http://www.w3-edge.com/wordpress-plugins/

Database Caching using memcached
Object Caching 4544/4862 objects using memcached
Content Delivery Network via a.dilcdn.com/bl

 Served from: blogs.disney.com @ 2015-03-30 04:50:26 by W3 Total Cache -->

<?php get_footer(); ?>
